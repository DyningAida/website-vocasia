<nav class="navbar-menu">
    <a class="menu navbar-brand" href="#">
        <img src="/img/logo_vocasia.png" alt="">
    </a>
    <a class="menu kategori btn" href="#">
        <img src="/img/kategori.png" alt=""> kategori
    </a>
    <a href="#" class="menu search">
        <input type="text" name="search" placeholder="Mau belajar apa?">
    </a>
    <div class="menu program dropdown">
        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
            Program
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
            <li><a class="dropdown-item" href="#">Action</a></li>
            <li><a class="dropdown-item" href="#">Another action</a></li>
            <li><a class="dropdown-item" href="#">Something else here</a></li>
        </ul>
    </div>
    <a class="menu cart btn" href="#">
        <img src="/img/cart.png" alt="" width="20px" height="20px">
    </a>
    <a class="menu masuk btn" href="#">
        Masuk
    </a>
    <a class="menu daftar btn" href="#">
        Daftar
    </a>
</nav>