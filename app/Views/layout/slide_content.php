<style>
    .w3-left,
    .w3-right,
    .w3-badge {
        cursor: pointer
    }

    .w3-badge {
        background-color: #A7D0E0;
        height: 13px;
        width: 13px;
        padding: 0;
    }
</style>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-akademik" role="tabpanel" aria-labelledby="nav-akademik-tab">
        <div class="w3-content">
            <div class="slide item akademik mySlides">
                <!-- nanti ini pakai foreach -->
                <?php for ($i = 0; $i < 4; $i++) :  ?>
                    <div class="content-item satu">
                        <div class="card" style="width: auto;">
                            <img src="/img/akademik-1.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Kiat Pemasaran Produk Secara Digital Untuk Sukses</h5>
                                <p class="card-text">Rp 150.000,00.</p>
                                <hr>
                                <div class="bottom">
                                    <img src="/img/photo.png" alt=""> Farid Subkhan
                                    <img src="/img/star.png" alt="">4.7
                                    <img class="user" src="/img/user.png" alt="">94 Siswa
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>
            <!-- slide kedua -->
            <div class="slide item akademik mySlides">
                <!-- nanti ini pakai foreach -->
                <?php for ($i = 0; $i < 4; $i++) :  ?>
                    <div class="content-item satu">
                        <div class="card" style="width: auto;">
                            <img src="/img/photo3.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Kiat Pemasaran Produk Secara Digital Untuk Sukses</h5>
                                <p class="card-text">Rp 150.000,00.</p>
                                <hr>
                                <div class="bottom">
                                    <img src="/img/photo.png" alt=""> Farid Subkhan
                                    <img src="/img/star.png" alt="">4.7
                                    <img class="user" src="/img/user.png" alt="">94 Siswa
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>
            <!-- slide ketiga -->
            <div class="slide item akademik mySlides">
                <!-- nanti ini pakai foreach -->
                <?php for ($i = 0; $i < 4; $i++) :  ?>
                    <div class="content-item satu">
                        <div class="card" style="width: auto;">
                            <img src="/img/akademik-1.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Kiat Pemasaran Produk Secara Digital Untuk Sukses</h5>
                                <p class="card-text">Rp 150.000,00.</p>
                                <hr>
                                <div class="bottom">
                                    <img src="/img/photo.png" alt=""> Farid Subkhan
                                    <img src="/img/star.png" alt="">4.7
                                    <img class="user" src="/img/user.png" alt="">94 Siswa
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>
            <!-- slide keempat -->
            <div class="slide item akademik mySlides">
                <!-- nanti ini pakai foreach -->
                <?php for ($i = 0; $i < 4; $i++) :  ?>
                    <div class="content-item satu">
                        <div class="card" style="width: auto;">
                            <img src="/img/photo3.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Kiat Pemasaran Produk Secara Digital Untuk Sukses</h5>
                                <p class="card-text">Rp 150.000,00.</p>
                                <hr>
                                <div class="bottom">
                                    <img src="/img/photo.png" alt=""> Farid Subkhan
                                    <img src="/img/star.png" alt="">4.7
                                    <img class="user" src="/img/user.png" alt="">94 Siswa
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>
        </div>
        <div class="slider w3-center">
            <?php for ($i = 1; $i < 5; $i++) : ?>
                <span class="w3-badge demo w3-border" onclick="currentDiv(<?= $i; ?>)"></span>
            <?php endfor; ?>
            <br>
            <a class="mt-5" onclick="plusDivs(1)">Lihat lebih banyak lagi <img src="/img/right-arrow.png" alt="" width="15px" height="10px"></a>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-bisnis" role="tabpanel" aria-labelledby="nav-bisnis-tab">
        <div class="w3-content">
            <div class="slide item bisnis mySlides">
                <!-- nanti ini pakai foreach -->
                <?php for ($i = 0; $i < 4; $i++) :  ?>
                    <div class="content-item satu">
                        <div class="card" style="width: auto;">
                            <img src="/img/akademik-1.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Kiat Pemasaran Produk Secara Digital Untuk Sukses</h5>
                                <p class="card-text">Rp 150.000,00.</p>
                                <hr>
                                <div class="bottom">
                                    <img src="/img/photo.png" alt=""> Farid Subkhan
                                    <img src="/img/star.png" alt="">4.7
                                    <img class="user" src="/img/user.png" alt="">94 Siswa
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>
            <!-- slide kedua -->
            <div class="slide item bisnis mySlides">
                <!-- nanti ini pakai foreach -->
                <?php for ($i = 0; $i < 4; $i++) :  ?>
                    <div class="content-item satu">
                        <div class="card" style="width: auto;">
                            <img src="/img/photo2.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Kiat Pemasaran Produk Secara Digital Untuk Sukses</h5>
                                <p class="card-text">Rp 150.000,00.</p>
                                <hr>
                                <div class="bottom">
                                    <img src="/img/photo.png" alt=""> Farid Subkhan
                                    <img src="/img/star.png" alt="">4.7
                                    <img class="user" src="/img/user.png" alt="">94 Siswa
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>
            <!-- slide ketiga -->
            <div class="slide item bisnis mySlides">
                <!-- nanti ini pakai foreach -->
                <?php for ($i = 0; $i < 4; $i++) :  ?>
                    <div class="content-item satu">
                        <div class="card" style="width: auto;">
                            <img src="/img/akademik-1.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Kiat Pemasaran Produk Secara Digital Untuk Sukses</h5>
                                <p class="card-text">Rp 150.000,00.</p>
                                <hr>
                                <div class="bottom">
                                    <img src="/img/photo.png" alt=""> Farid Subkhan
                                    <img src="/img/star.png" alt="">4.7
                                    <img class="user" src="/img/user.png" alt="">94 Siswa
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>
        </div>
        <div class="w3-center">
            <div class="slider w3-center">
                <?php for ($i = 1; $i < 5; $i++) : ?>
                    <span class="w3-badge demo w3-border" onclick="currentDiv(<?= $i; ?>)"></span>
                <?php endfor; ?>
                <br>
                <a class="mt-5" onclick="plusDivs(1)">Lihat lebih banyak lagi <img src="/img/right-arrow.png" alt="" width="15px" height="10px"></a>
            </div>
        </div>
    </div>


    <!-- slide pemasaran -->
    <div class="tab-pane fade" id="nav-pemasaran" role="tabpanel" aria-labelledby="nav-pemasaran-tab">
        <!-- slide 1     -->
        <div class="w3-content">
            <div class="slide item pemasaran mySlides">
                <!-- nanti ini pakai foreach -->
                <?php for ($i = 0; $i < 4; $i++) :  ?>
                    <div class="content-item satu">
                        <div class="card" style="width: auto;">
                            <img src="/img/akademik-1.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Kiat Pemasaran Produk Secara Digital Untuk Sukses</h5>
                                <p class="card-text">Rp 150.000,00.</p>
                                <hr>
                                <div class="bottom">
                                    <img src="/img/photo.png" alt=""> Farid Subkhan
                                    <img src="/img/star.png" alt="">4.7
                                    <img class="user" src="/img/user.png" alt="">94 Siswa
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>

            <!-- slide 2 -->
            <div class="slide item pemasaran mySlides">
                <!-- nanti ini pakai foreach -->
                <?php for ($i = 0; $i < 4; $i++) :  ?>
                    <div class="content-item satu">
                        <div class="card" style="width: auto;">
                            <img src="/img/photo3.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Kiat Pemasaran Produk Secara Digital Untuk Sukses</h5>
                                <p class="card-text">Rp 150.000,00.</p>
                                <hr>
                                <div class="bottom">
                                    <img src="/img/photo.png" alt=""> Farid Subkhan
                                    <img src="/img/star.png" alt="">4.7
                                    <img class="user" src="/img/user.png" alt="">94 Siswa
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>
            <!-- slide 3 -->
            <div class="slide item pemasaran mySlides">
                <!-- nanti ini pakai foreach -->
                <?php for ($i = 0; $i < 4; $i++) :  ?>
                    <div class="content-item satu">
                        <div class="card" style="width: auto;">
                            <img src="/img/photo2.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Kiat Pemasaran Produk Secara Digital Untuk Sukses</h5>
                                <p class="card-text">Rp 150.000,00.</p>
                                <hr>
                                <div class="bottom">
                                    <img src="/img/photo.png" alt=""> Farid Subkhan
                                    <img src="/img/star.png" alt="">4.7
                                    <img class="user" src="/img/user.png" alt="">94 Siswa
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>
            <!-- slide 4 -->
            <div class="slide item pemasaran mySlides">
                <!-- nanti ini pakai foreach -->
                <?php for ($i = 0; $i < 4; $i++) :  ?>
                    <div class="content-item satu">
                        <div class="card" style="width: auto;">
                            <img src="/img/akademik-1.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Kiat Pemasaran Produk Secara Digital Untuk Sukses</h5>
                                <p class="card-text">Rp 150.000,00.</p>
                                <hr>
                                <div class="bottom">
                                    <img src="/img/photo.png" alt=""> Farid Subkhan
                                    <img src="/img/star.png" alt="">4.7
                                    <img class="user" src="/img/user.png" alt="">94 Siswa
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>
        </div>
        <div class="w3-center">
            <div class="slider w3-center">
                <?php for ($i = 1; $i < 5; $i++) : ?>
                    <span class="w3-badge demo w3-border" onclick="currentDiv(<?= $i; ?>)"></span>
                <?php endfor; ?>
                <br>
                <a class="mt-5" onclick="plusDivs(1)">Lihat lebih banyak lagi <img src="/img/right-arrow.png" alt="" width="15px" height="10px"></a>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-ilkom" role="tabpanel" aria-labelledby="nav-ilkom-tab">
        <div class="slide item ilmu_komputer">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt, eum!</p>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-fotovideo" role="tabpanel" aria-labelledby="nav-fotovideo-tab">
        <div class="slide item foto_video">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa, veritatis!</p>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-lifestyle" role="tabpanel" aria-labelledby="nav-lifestyle-tab">
        <div class="slide item life_style">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellat, voluptas?</p>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-desain" role="tabpanel" aria-labelledby="nav-desain-tab">
        <div class="slide item desain">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis, ut.</p>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-pemasarann" role="tabpanel" aria-labelledby="nav-pemasarann-tab">
        <div class="slide item pemasarann">
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Unde, quis.</p>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-all" role="tabpanel" aria-labelledby="nav-all-tab">
        <div class="slide item semua">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis, aliquam.</p>
        </div>
    </div>
</div>