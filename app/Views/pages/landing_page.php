<?= $this->extend('layout/template'); ?>
<?= $this->section('content-1'); ?>
<div class="text w3-left">
    <p class="upgrade-skill">Yuk Siapkan Dirimu Mulai <a href="#">Upgrade Skill</a> Bersama Vocasia</p>
    <p class="text-info">Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime officiis consequatur animi necessitatibus culpa iusto expedita aspernatur facilis magnam dignissimos?</p>
    <div class="for-button">
        <a href="#" class="btn register">Daftar gratis</a>
        <a href="#" class="btn instructor">Jadi Instruktur</a>
    </div>
</div>
<div class="picture w3-right">
    <img src="/img/illust-4.png" alt="" class="mt-5" style="margin-left: 150px;">
</div>
<?= $this->endSection(); ?>
<?= $this->section('slide-show'); ?>
<div class="nav-tab-content">
    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <button class="nav-link active" id="nav-akademik-tab" data-bs-toggle="tab" data-bs-target="#nav-akademik" type="button" role="tab" aria-controls="nav-akademik" aria-selected="true">Akademik</button>
            <button class="nav-link" id="nav-bisnis-tab" data-bs-toggle="tab" data-bs-target="#nav-bisnis" type="button" role="tab" aria-controls="nav-bisnis" aria-selected="false">Bisnis</button>
            <button class="nav-link" id="nav-pemasaran-tab" data-bs-toggle="tab" data-bs-target="#nav-pemasaran" type="button" role="tab" aria-controls="nav-pemasaran" aria-selected="false">Pemasaran</button>
            <button class="nav-link" id="nav-ilkom-tab" data-bs-toggle="tab" data-bs-target="#nav-ilkom" type="button" role="tab" aria-controls="nav-ilkom" aria-selected="false">Ilmu Komputer</button>
            <button class="nav-link" id="nav-fotovideo-tab" data-bs-toggle="tab" data-bs-target="#nav-fotovideo" type="button" role="tab" aria-controls="nav-fotovideo" aria-selected="false">Foto & Videografi</button>
            <button class="nav-link" id="nav-lifestyle-tab" data-bs-toggle="tab" data-bs-target="#nav-lifestyle" type="button" role="tab" aria-controls="nav-lifestyle" aria-selected="false">Lifestyle</button>
            <button class="nav-link" id="nav-desain-tab" data-bs-toggle="tab" data-bs-target="#nav-desain" type="button" role="tab" aria-controls="nav-desain" aria-selected="false">Desain</button>
            <button class="nav-link" id="nav-pemasarann-tab" data-bs-toggle="tab" data-bs-target="#nav-pemasarann" type="button" role="tab" aria-controls="nav-pemasarann" aria-selected="false">Pemasaran</button>
            <button class="nav-link" id="nav-all-tab" data-bs-toggle="tab" data-bs-target="#nav-all" type="button" role="tab" aria-controls="nav-all" aria-selected="false">Semua Kategori</button>
        </div>
    </nav>
    <?= $this->include('layout/slide_content'); ?>
</div>
<?= $this->endSection(); ?>