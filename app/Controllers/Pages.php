<?php

namespace App\Controllers;

class Pages extends BaseController
{
    public function index()
    {
        $data = [
            'title' => 'landing-page',
        ];
        return view('pages/landing_page', $data);
    }
}
